package com.want.api.model.form;

public class PreOrderForm {

    private String token;
    private String products;
    private int category;
    private long created_at;
    private long duration;
    private String phone;
    private String address;

    public PreOrderForm() {
    }

    public PreOrderForm(String token) {
        this.token = token;
    }

    public String getProducts() {
        return products;
    }

    public void setProducts(String products) {
        this.products = products;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public void setCreated_at(long created_at) {
        this.created_at = created_at;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getToken() {
        return token;
    }

    public long getCreationDate() {
        return created_at;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public long getCreated_at() {
        return created_at;
    }
}