package com.want.api.model;

import com.want.api.model.form.PreOrderForm;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "request")
public class PreOrder {

    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    private long id;

    @Column(name = "token", nullable = false)
    private String token;

    @Column(name = "product", nullable = false)
    private String product;

    @Column(name = "created_at", nullable = false)
    private long createdAt;

    @Column(name = "duration", nullable = false)
    private long duration;

    @Column(name = "phone", nullable = false)
    private String phone;

    @Column(name = "address", nullable = false)
    private String address;

    @Column(name = "offerIds")
    private String offerIds;

    public PreOrder() {
    }

    public PreOrder(PreOrderForm form) {
        token = form.getToken();
        product = form.getProducts();
        createdAt = form.getCreationDate();
        duration = form.getDuration();
        phone = form.getPhone();
        address = form.getAddress();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getOfferIds() {
        return offerIds;
    }

    public void setOfferIds(String offerIds) {
        this.offerIds = offerIds;
    }
}
