package com.want.api.controller;

import com.want.api.model.PreOrder;
import com.want.api.model.form.PreOrderForm;
import com.want.api.service.PreOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PreOrderController {

    @Autowired
    private PreOrderService service;

    @CrossOrigin
    @ResponseBody
    @RequestMapping(value = "/order/pre/",method = RequestMethod.POST)
    public PreOrder addRequest(@RequestBody PreOrderForm form){
        return service.create(form);
    }

    @CrossOrigin
    @ResponseBody
    @RequestMapping(value = "/order/{token}",method = RequestMethod.GET)
    public List<PreOrder> all(@PathVariable("token") String token){
        return service.all(token);
    }

    @CrossOrigin
    @ResponseBody
    @RequestMapping(value = "/order/{id}/{token}",method = RequestMethod.GET)
    public PreOrder one(@PathVariable("id") long id,@PathVariable("token") String token){
        return service.one(id,token);
    }
}
