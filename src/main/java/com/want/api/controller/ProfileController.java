package com.want.api.controller;

import com.want.api.model.Profile;
import com.want.api.service.ProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class ProfileController {

    @Autowired
    private ProfileService service;

    @CrossOrigin
    @ResponseBody
    @RequestMapping(value = "/profile/",method = RequestMethod.POST)
    public Profile create(@RequestBody Profile profile){
        return service.create(profile);
    }

}
