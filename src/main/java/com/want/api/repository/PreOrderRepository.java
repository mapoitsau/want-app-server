package com.want.api.repository;

import com.want.api.model.PreOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

public interface PreOrderRepository extends JpaRepository<PreOrder,Long> {

    @Query(value = "select * from request where token=:token",nativeQuery = true)
    List<PreOrder> find(@Param("token") String token);

    @Query(value = "select * from request where token=:token and id=:id",nativeQuery = true)
    PreOrder find(@Param("id") long id,@Param("token") String token);

}
