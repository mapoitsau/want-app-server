package com.want.api.service;

import com.want.api.model.PreOrder;
import com.want.api.model.form.PreOrderForm;
import com.want.api.repository.PreOrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PreOrderServiceImpl implements PreOrderService {

    @Autowired
    private PreOrderRepository repository;

    @Override
    public PreOrder create(PreOrderForm form) {
        return repository.save(new PreOrder(form));
    }

    @Override
    public List<PreOrder> all(String token) {
        return repository.find(token);
    }

    @Override
    public PreOrder one(long id, String token) {
        return repository.find(id,token);
    }
}
