package com.want.api.service;

import com.want.api.model.Profile;

public interface ProfileService {

    Profile create(Profile profile);
}
