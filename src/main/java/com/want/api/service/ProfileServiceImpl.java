package com.want.api.service;

import com.want.api.model.Profile;
import com.want.api.repository.ProfileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProfileServiceImpl implements ProfileService {

    @Autowired
    private ProfileRepository repository;

    @Override
    public Profile create(Profile profile) {
        return repository.save(profile);
    }
}
