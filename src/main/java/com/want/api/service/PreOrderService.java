package com.want.api.service;

import com.want.api.model.PreOrder;
import com.want.api.model.form.PreOrderForm;

import java.util.List;

public interface PreOrderService {

    PreOrder create(PreOrderForm form);
    List<PreOrder> all(String token);
    PreOrder one(long id,String token);
}
